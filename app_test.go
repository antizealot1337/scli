package scli

import "testing"

func TestApp_chechCmds(t *testing.T) {
	var a App

	err := a.checkCmds()

	if err != nil {
		t.Error("unexpected error with no commands", err)
	} //if

	a.Cmds = append(a.Cmds, &Cmd{Name: "a"})

	err = a.checkCmds()

	if err != nil {
		t.Error("unexpected error with one command", err)
	} //if

	a.Cmds = append(a.Cmds, &Cmd{Name: "a"})

	err = a.checkCmds()

	if err == nil {
		t.Error("expected error for duplicate command")
	} //if
} //func

func TestApp_runFlags(t *testing.T) {
} //func

func TestAppRun__runSubCmd(t *testing.T) {
	sub := false
	app := App{
		Cmds: []*Cmd{
			&Cmd{
				Name: "a",
				Subs: []*Cmd{
					&Cmd{
						Name: "1",
						Exec: func([]string) error {
							sub = true
							return nil
						}, //func
					}, //struct
				}, //slice
			}, //struct
		}, //slice
		Exec: func([]string) error {
			return nil
		}, //func
	} //struct

	err := app.RunWith([]string{"", "a", "1"})

	if err != nil {
		t.Error(err)
	} //if

	if !sub {
		t.Error("expected sub command to be run")
	} //if
} //func

func TestAppRunWith__exec_default(t *testing.T) {
	def, sub := false, false

	app := App{
		Exec: func([]string) error {
			def = true
			return nil
		},
		Cmds: []*Cmd{
			&Cmd{
				Name: "a",
				Exec: func([]string) error {
					sub = true
					return nil
				},
			},
		},
	}

	err := app.RunWith([]string{""})

	if err != nil {
		t.Fatal(err)
	} //if

	if sub {
		t.Error("did not expect the command to run")
	} //if

	if !def {
		t.Error("expected the default to run")
	} //if
} //func

func TestAppRunWith__exec_subcommand(t *testing.T) {
	def, sub := false, false

	app := App{
		Exec: func([]string) error {
			def = true
			return nil
		},
		Cmds: []*Cmd{
			&Cmd{
				Name: "a",
				Exec: func([]string) error {
					sub = true
					return nil
				},
			},
		},
	}

	err := app.RunWith([]string{"", "a"})

	if err != nil {
		t.Fatal(err)
	} //if

	if !sub {
		t.Error("expected the command to run")
	} //if

	if def {
		t.Error("did not expect the default to run")
	} //if
}
