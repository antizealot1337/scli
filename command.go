package scli

// Command is a representation "subcommands" present on the command line.
type Command interface {
	Matches(s string) bool
	Run(args []string) error
} //interface
