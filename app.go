package scli

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"unicode/utf8"

	flag "bitbucket.org/antizealot1337/pflag"
	"golang.org/x/xerrors"
)

// App is the root of a scli command line application. It has fields to display
// when help is called and optional commands and a default function to execute
// if no commands are run.
type App struct {
	Name    string
	Version string
	Desc    string
	Flags   flag.FlagSet
	Cmds    []*Cmd
	Exec    func(args []string) error
} //struct

func (a *App) checkCmds() error {
	names := make(map[string]bool)

	for _, cmd := range a.Cmds {
		_, ok := names[cmd.Name]

		if ok {
			return xerrors.Errorf("duplicate command \"%s", cmd.Name)
		} //if

		names[cmd.Name] = true
	} //for

	return nil
} //func

func (a *App) runArgs(args []string) error {
	if a.Name == "" {
		a.Name = filepath.Base(args[0])
	} //if

	// Init the flags
	a.Flags.Init(a.Name, flag.ContinueOnError)
	//a.Flags.Init(a.Name, flag.ExitOnError)

	// Add a version flag
	showVer := false
	if a.Version != "" {
		a.Flags.BoolVar(&showVer, "version", showVer, "Show the version")
	} //if

	// TODO: Set the usage
	a.Flags.Usage = func() {
		a.WriteUsage(os.Stdout)
		os.Exit(0)
	} // func

	// Parse the flags
	err := a.Flags.Parse(args[1:])

	if err != nil {
		// TODO: Do error checking
		return nil
	} //if

	// Check for the version flag
	if showVer {
		fmt.Println(a.Name, a.Version)
		os.Exit(0)
		return nil
	} //if

	if a.Flags.NArg() == 0 {
		return nil
	} //if

	return nil
} //func

func (a *App) runCmds() (bool, error) {
	// Loop through the commands
	for _, cmd := range a.Cmds {
		// Check if the command matches
		if cmd.Matches(a.Flags.Arg(0)) {
			return true, cmd.Run(a.Flags.Args())
		} //if
	} //for

	return false, xerrors.New("command not found")
} //func

// RunWith runs the App with the supplied args. That args are first parsed
// through the flags with the remaining passed to a command or the Exec func.
// The args are expected to be in the form of ["executable name",
// "flags and args"...].
func (a *App) RunWith(args []string) (err error) {
	err = a.checkCmds()

	if err != nil {
		return xerrors.Errorf("run error: %v", err)
	} //if

	err = a.runArgs(args)

	if err != nil {
		return xerrors.Errorf("run error: %v", err)
	} //if

	switch {
	case len(a.Cmds) > 0 && a.Flags.NArg() > 0:
		ran, err := a.runCmds()

		switch {
		case ran && err != nil:
			return err
		case !ran && a.Exec != nil:
			return a.Exec(a.Flags.Args())
		default:
		}

		if ran && err != nil {
			return err
		} //if

		if !ran && a.Exec != nil {
			return a.Exec(a.Flags.Args())
		} //if
	case a.Exec != nil:
		err = a.Exec(a.Flags.Args())
	default:
		a.Flags.Usage()
	} //switch

	if err != nil {
		return xerrors.Errorf("run error: %v", err)
	} //if

	return nil
} //func

// Run is the same as RunWith(os.Args)
func (a *App) Run() (err error) {
	return a.RunWith(os.Args)
} //func

// WriteUsage will write the usage to the supplied io.Writer.
func (a *App) WriteUsage(out io.Writer) error {
	fmt.Fprintln(out, a.Name, a.Version)
	fmt.Fprintln(out)
	fmt.Fprintln(out, "Description:", a.Desc)
	fmt.Fprintln(out)
	if a.Flags.NFlag() > 0 {
		fmt.Fprintln(out, "Usage:", filepath.Base(os.Args[0]), "[flags] command")
	} else {
		fmt.Fprintln(out, "Usage:", filepath.Base(os.Args[0]), "command")
	} //if
	fmt.Fprintln(out)
	fmt.Fprintln(out, "Commands:")
	a.writeCmds(out)

	// Check for flags before writing the flags section
	if a.Flags.NFlag() > 0 {
		fmt.Fprintln(out)
		fmt.Fprintln(out, "Flags:")
		a.Flags.SetOutput(out)
		a.Flags.PrintDefaults()
	} //if

	return nil
} //func

func (a *App) writeCmds(w io.Writer) error {
	padding := 0

	for _, cmd := range a.Cmds {
		nl := utf8.RuneCountInString(cmd.Name)
		if nl > padding {
			padding = nl
		} //if
	} //for

	padding += 2

	for _, cmd := range a.Cmds {
		cmd.writeUsage(w, padding+2)
		fmt.Println()
	} //for

	return nil
} //func
