module bitbucket.org/antizealot1337/scli

go 1.12

require (
	bitbucket.org/antizealot1337/pflag v0.0.0-20200116210133-9693d4bd1bd7
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
)
