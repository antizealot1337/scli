package scli

import (
	"fmt"
	"io"
	"strings"
	"unicode/utf8"

	"golang.org/x/xerrors"
)

// Cmd is a command that can be run from an App.
type Cmd struct {
	Name string
	Desc string
	Exec func(args []string) error
	Subs []*Cmd
} //struct

// Matches checks if the command should run based on the supplied string.
func (c *Cmd) Matches(s string) bool {
	return c.Name == s
} //func

func (c *Cmd) exec(args []string) error {
	if c.Exec == nil {
		return xerrors.Errorf("command %s has no exec func", c.Name)
	} //if

	return c.Exec(args)
} //func

// Run executes the command with the supplied args.
func (c *Cmd) Run(args []string) error {
	// Check for args which are needed to match the name of a sub command
	if len(args) > 1 {
		// Loop through the sub commands
		for _, cmd := range c.Subs {
			// Check if the current command matches the second arg
			// which would be the command since the args should
			// look like [my name, command name, the rest...]
			if cmd.Matches(args[1]) {
				return cmd.Run(args[1:])
			} //if
		} //for
	} //if

	return c.exec(args)
} //func

func (c *Cmd) writeUsage(w io.Writer, padding int) error {
	fmt.Fprint(w, "\t", c.Name,
		strings.Repeat(" ", padding-utf8.RuneCountInString(c.Name)), c.Desc)
	return nil
} //func
