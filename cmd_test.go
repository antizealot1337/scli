package scli

import (
	"testing"
)

var _ Command = (*Cmd)(nil)

func TestCmdMatches(t *testing.T) {
	c := Cmd{
		Name: "Cmd",
	} //struct

	if s := ""; c.Matches(s) {
		t.Errorf(`unexpected match "%s"`, s)
	} //if

	if s := "no match"; c.Matches(s) {
		t.Errorf(`unexpected match "%s"`, s)
	} //if

	if s := c.Name; !c.Matches(s) {
		t.Errorf(`expected match "%s"`, s)
	} //if
} //func

func TestCmdRun__NoExec(t *testing.T) {
	var c Cmd

	err := c.Run(nil)

	if err == nil {
		t.Fatal("expected missing exec error")
	} //if
} //func

func TestCmdRun__ExecRun(t *testing.T) {
	ran := false

	c := Cmd{
		Exec: func([]string) error {
			ran = true
			return nil
		},
	}

	err := c.Run(nil)

	if err != nil {
		t.Error(err)
	} //if

	if !ran {
		t.Error("Exec not run")
	} //if
} //func

func TestCmdRun_SubCmds(t *testing.T) {
	ransub := false
	ranmain := false

	c := Cmd{
		Name: "cmd",
		Exec: func([]string) error {
			ranmain = true
			return nil
		},
		Subs: []*Cmd{
			&Cmd{
				Name: "a",
				Exec: func([]string) error {
					ransub = true
					return nil
				},
			},
		},
	}

	err := c.Run([]string{"cmd", "a"})

	if err != nil {
		t.Fatal(err)
	} //if

	if !ransub {
		t.Error("did not run sub command")
	} //if

	err = c.Run([]string{"cmd", "b"})

	if err != nil {
		t.Fatal(err)
	} //if

	if !ranmain {
		t.Error("did not run main command")
	} //if
} //func
